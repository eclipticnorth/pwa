(function () {
  'use strict';

  var app = {
    isLoading: true,
    visibleCards: {},
    spinner: document.querySelector('.loader'),
    cardTemplate: document.querySelector('.cardTemplate'),
    container: document.querySelector('.main'),
    pushButton: document.querySelector('.js-push-btn'),
    applicationServerPublicKey: 'BLCCZGP5EnvLLht7_Sa8g9XeIsJkVouj6mjTRzOnD_Xq2wQ3Sa6PZYco70_HMHpzp7d_WCwAC25A9dwVhPdwx7A',
    isSubscribed: false
  };


  /*****************************************************************************
   *
   * Event listeners for UI elements
   *
   ****************************************************************************/

  document.getElementById('butRefresh').addEventListener('click', function () {
    // Refresh all of the forecasts
    app.getLatestTemperature();
  });


  /*****************************************************************************
   *
   * Initilize State
   *
   ****************************************************************************/
  app.initializeUI = () => {
    app.pushButton.addEventListener('click', () => {
      app.pushButton.disabled = true;
      if (app.isSubscribed) {
        // TODO: Unsubscribe user
        unsubscribeUser();
      } else {
        app.subscribeUser();
      }

    })

    // Set the initial subscription value
    app.swRegistration.pushManager.getSubscription()
      .then((subscription) => {
        app.isSubscribed = !(subscription === null);

        updateSubscriptionOnServer(subscription);

        app.isSubscribed ? console.log('User IS subscribed') : console.log('User is NOT subscribed');

        app.updateBtn();
      });
  }

  app.subscribeUser = () => {
    const applicationServerKey = urlB64ToUint8Array(app.applicationServerPublicKey);

    app.swRegistration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: applicationServerKey
    })
      .then((subscription) => {
        console.log('User is subscribed.');

        updateSubscriptionOnServer(subscription);

        app.isSubscribed = true;

        app.updateBtn();
      })
      .catch((err) => {
        console.log('Failed to subscribe the user:', err);
        app.updateBtn();
      });
  }

  function unsubscribeUser() {
    app.swRegistration.pushManager.getSubscription()
      .then((subscription) => {
        if (subscription) {
          return subscription.unsubscribe();
        }
      })
      .catch((err) => {
        console.log('Error unsubscribing', err);
      })
      .then(() => {
        updateSubscriptionOnServer(null);

        console.log('User is unsubscribed.');
        app.isSubscribed = false;

        app.updateBtn();
      });
  }

  function updateSubscriptionOnServer(subscription) {
    // TODO: Send subscription to application server

    if (subscription) {
      console.log(JSON.stringify(subscription));
    }
  }



  app.updateBtn = () => {
    // TODO: handle permission denied for push messages
    if (Notification.permission === 'denied') {
      app.pushButton.textContent = 'Push Messaging Blocked';
      app.pushButton.disabled = true;
      updateSubscriptionOnServer(null);
      return;
    }

    if (app.isSubscribed) {
      app.pushButton.textContent = 'Disable Push Messaging';
    } else {
      app.pushButton.textContent = 'Enable Push Messaging';
    }

    app.pushButton.disabled = false;
  }


  /*****************************************************************************
   *
   * Methods to update/refresh the UI
   *
   ****************************************************************************/


  // Updates a weather card with the latest weather forecast. If the card
  // doesn't already exist, it's cloned from the template.
  app.updateTemperatureCard = function (data) {
    var dataLastUpdated = data.created;
    var current = data.condition;

    var card = app.visibleCards[data.key];
    if (!card) {
      card = app.cardTemplate.cloneNode(true);
      card.classList.remove('cardTemplate');
      card.querySelector('.location').textContent = data.location;
      card.removeAttribute('hidden');
      app.container.appendChild(card);
      app.visibleCards[data.key] = card;
    }

    // Verifies the data provide is newer than what's already visible
    // on the card, if it's not bail, if it is, continue and update the
    // time saved in the card
    var cardLastUpdatedElem = card.querySelector('.card-last-updated');
    var cardLastUpdated = cardLastUpdatedElem.textContent;
    if (cardLastUpdated) {
      cardLastUpdated = cardLastUpdated;
      // Bail if the card has more recent data then the data
      if (dataLastUpdated < cardLastUpdated) {
        return;
      }
    }
    cardLastUpdatedElem.textContent = data.created;

    card.querySelector('.date').textContent = app.getFormattedDate(new Date(+data.created));;
    card.querySelector('.current .temperature .value').textContent = Math.round(current.temperature);
    card.querySelector('.current .humidity').textContent = Math.round(current.humidity) + '%';
    if (app.isLoading) {
      app.spinner.setAttribute('hidden', true);
      app.container.removeAttribute('hidden');
      app.isLoading = false;
    }
  };



  /*****************************************************************************
   *
   * Methods for dealing with the model
   *
   ****************************************************************************/

  /*
   * Gets a forecast for a specific city and updates the card with the data.
   * getLatestTemperature() first checks if the weather data is in the cache. If so,
   * then it gets that data and populates the card with the cached data.
   * Then, getLatestTemperature() goes to the network for fresh data. If the network
   * request goes through, then the card gets updated a second time with the
   * freshest data.
   */
  app.getLatestTemperature = function () {
    var url = 'https://agty5ar6f4.execute-api.us-east-1.amazonaws.com/prod/readings'
    // TODO add cache logic here
    if ('caches' in window) {
      caches.match(url).then((response) => {
        if (response) {
          response.json().then(function updateFromCache(json) {
            var results = json.body.Items[0];
            var data = {};
            data.key = results.Location;
            data.location = results.Location;
            data.created = +results.CreateDate;
            data.condition = { temperature: results.Condition.Temperature, humidity: results.Condition.Humidity }
            app.updateTemperatureCard(data);
          })
        }
      })
    }

    // Fetch the latest data.
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
      if (request.readyState === XMLHttpRequest.DONE) {
        if (request.status === 200) {
          var response = JSON.parse(request.response);
          var results = response.body.Items[0];
          var data = {};
          data.key = results.Location;
          data.location = results.Location;
          data.created = +results.CreateDate;
          data.condition = { temperature: results.Condition.Temperature, humidity: results.Condition.Humidity }
          app.updateTemperatureCard(data);
        }
      } else {
        // Return the initial weather forecast since no data is available.
        app.updateTemperatureCard(initalTemperatureData);
      }
    };
    request.open('GET', url);
    request.send();
  };


  /*****************************************************************************
   *
   * Sample data to show UI
   *
   ****************************************************************************/
  var initalTemperatureData = {
    key: 'Freezer',
    location: 'Freezer',
    condition: {
      temperature: 12.5,
      humidity: 45,
    },
    created: new Date('2016-07-22T01:00:00Z').getTime()
  };

  app.getFormattedDate = (d) => {
    if (d instanceof Date && !isNaN(d))
      return `${pad(d.getMonth() + 1)}/${pad(d.getDate())}/${d.getFullYear()} ${pad(d.getHours())}${pad(d.getMinutes())}`;

    return 'Invalid Date';

    function pad(num) {
      return num.toString().padStart(2, '0');
    }
  }

  function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }



  // TODO uncomment line below to test app with fake data
  // app.updateTemperatureCard(initalTemperatureData);

  // TODO add startup code here
  app.getLatestTemperature();

  app.swRegistration = {};

  // TODO add service worker code here
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
      .register('./service-worker.js')
      .then((swReg) => {
        console.log('Service Worker Registered', swReg)

        app.swRegistration = swReg;
        app.initializeUI();
      });
  }

  // TODO verify push is supported in browser
  if (!'PushManager' in window) {
    console.warn('Push messaging is not supported');
    app.pushButton.textContent = 'Push Not Supported';
  }
})();
